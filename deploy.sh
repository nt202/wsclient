#!/bin/bash

ARCHIVE_FILE=ws.nt202.ru.tar.gz;

# Variables:
REPO_NAME=ws

mkdir -p /data;

(cd /data && rm -rf $REPO_NAME)
(cd /data && git clone https://gitlab-ci-token:$GITLAB_PERSONAL_ACCESS_TOKEN@gitlab.com/nt202.ru/wsclient.git $REPO_NAME)
(cd /data/$REPO_NAME && tar -zcvf $ARCHIVE_FILE -C public index.html favicon.ico main.dart.js styles.css)
(cd /data/$REPO_NAME && scp -P 8022 $ARCHIVE_FILE root@nt202.ru:.)
(cd /data && rm -rf $REPO_NAME)

# Define the script as a string
SITE_DIR=/var/www/ws.nt202.ru
REMOTE_SCRIPT="
apt install -y nginx
cat > /etc/nginx/conf.d/ws.nt202.ru.conf << EOL
server {
    listen 80;
    server_name ws.nt202.ru;
    location / {
        root $SITE_DIR;
        index index.html;
    }
}
EOL
mkdir -p $SITE_DIR
(cd $SITE_DIR && mv ~/$ARCHIVE_FILE .)
(cd $SITE_DIR && tar -xzvf ./$ARCHIVE_FILE)
(cd $SITE_DIR && rm -rf ./$ARCHIVE_FILE)
systemctl restart nginx
rm -rf ~/$ARCHIVE_FILE
echo Success!
"

echo "$REMOTE_SCRIPT" | ssh -p 8022 root@nt202.ru "sh"

# run 6
