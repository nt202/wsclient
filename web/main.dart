import 'dart:async';
import 'dart:html';

import 'package:web_socket_channel/html.dart';
import 'package:http/http.dart' as http;

void main() {
  auth();
  ws();
}

void auth() {
  querySelector('#id_btn_auth')?.onClick.listen((event) async {
    final InputElement inputUrl = querySelector('#id_url_auth') as InputElement;
    final String? url = inputUrl.value;
    if (url == null || url.isEmpty) {
      return;
    }
    final InputElement inputToken = querySelector('#id_auth_token') as InputElement;
    final String? token = inputToken.value;
    if (token == null || token.isEmpty) {
      return;
    }
    print("url = $url");
    print("token = $token");
    var response = await http.get(Uri.parse(url), headers: {"Authorization": token});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
  });
}

void ws() {
  final StreamController<String> streamController = StreamController.broadcast(sync: true);
  HtmlWebSocketChannel? channel;
  bool hasWsConnection = false;

  querySelector('#id_btn_ws_close')?.onClick.listen((event) {
    if (channel != null) {
      channel?.sink.close(1000, "Client initiative").whenComplete(() => channel = null);
    }
  });

  querySelector('#id_btn_ws_connect')?.onClick.listen((event) {
    final InputElement inputUrl = querySelector('#id_url_ws') as InputElement;
    final String? url = inputUrl.value;
    if (url == null || url.isEmpty) {
      return;
    }
    channel = HtmlWebSocketChannel.connect(url.trim());
    hasWsConnection = true;
    channel!.stream.listen((event) {
      hasWsConnection = true;
      final String responseBody = event.toString();
      streamController.add(responseBody);
    }, onDone: () {
      print("WS: Connection aborted");
      hasWsConnection = false;
    }, onError: (e) {
      print("WS: Server error: $e");
      hasWsConnection = false;
    }, cancelOnError: true);
  });

  querySelector('#id_btn_ws_send')?.onClick.listen((event) {
    if (channel != null) {
      final String? requestBody = (querySelector("#id_request_body") as TextAreaElement).value;
      if (requestBody == null || requestBody.trim().isEmpty) {
        return;
      }
      channel!.sink.add(requestBody);
    }
  });

  Timer.periodic(Duration(seconds: 1), (timer) {
    if (hasWsConnection) {
      querySelector('#id_indicator')!.style.backgroundColor = "green";
    } else {
      querySelector('#id_indicator')!.style.backgroundColor = "red";
    }
    if (channel != null && channel!.closeCode != null) {
      (querySelector('#id_close_event') as InputElement).value = "${channel!.closeCode} ${channel!.closeReason}";
    } else {
      (querySelector('#id_close_event') as InputElement).value = "";
    }
  });
}
